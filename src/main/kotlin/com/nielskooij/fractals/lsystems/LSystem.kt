package com.nielskooij.fractals.lsystems

import processing.core.PGraphics
import java.util.*

class LSystem(
    private val graphics: PGraphics,
    private val rules: Map<String, String>,
    private val length: Float,
    private val axiom: String,
    private val settings: LSystemSettings
) {

    fun draw(maxDepth: Int, startX: Float, startY: Float) {
        var depth = 0

        var sequence: List<String> = axiom.map { it.toString() }
        while (depth < maxDepth) {
            sequence = createSequence(sequence)
            depth++
        }

        drawLSystem(startX, startY, sequence)
    }

    fun createSequence(sequence: List<String>): List<String> {
        val result: StringBuilder = StringBuilder()
        sequence.map {
            result.append(rules.getOrDefault(it, it))
        }
        return result.map { it.toString() }
    }

    private fun drawLSystem(startX: Float, startY: Float, sequence: List<String>) {
        val stack: Stack<LSystemState> = Stack()

        stack.push(
            LSystemState(
            settings.angleOffset,
            270f,
            startX,
            startY,
            1f,
            length,
            true
        )
        )

        sequence.forEach {
            when (it) {
                "F" -> {
                    val newX = newX(stack.peek().currentX, stack.peek().currentLineLength, stack.peek().currentAngle)
                    val newY = newY(stack.peek().currentY, stack.peek().currentLineLength, stack.peek().currentAngle)

                    line(stack.peek().currentX, stack.peek().currentY, newX, newY, stack.peek().currentLineWidth)
                    stack.peek().currentX = newX
                    stack.peek().currentY = newY
                }
                "f" -> {
                    stack.peek().currentX = newX(stack.peek().currentX, stack.peek().currentLineLength, stack.peek().currentAngle)
                    stack.peek().currentY = newY(stack.peek().currentY, stack.peek().currentLineLength, stack.peek().currentAngle)
                }
                "+" -> stack.peek().currentAngle += stack.peek().turningAngle //settings.angleOffset
                "-" -> stack.peek().currentAngle -= stack.peek().turningAngle //settings.angleOffset
                "|" -> stack.peek().currentAngle += 180f
                "#" -> stack.peek().currentLineWidth += settings.lineWidthOffset
                "!" -> stack.peek().currentLineWidth -= settings.lineWidthOffset
                ">" -> stack.peek().currentLineLength *= settings.lineLengthScalar
                "<" -> stack.peek().currentLineLength /= settings.lineLengthScalar
                "(" -> stack.peek().turningAngle += settings.turningAngleOffset
                ")" -> stack.peek().turningAngle -= settings.turningAngleOffset
                "[" -> stack.push(stack.peek().copy())
                "]" -> stack.pop()
                //TODO { }
                else -> println("Unknown Symbol: $it")
            }
        }
    }

    fun line(startX: Float, startY: Float, endX: Float, endY: Float, lineWidth: Float) {
        graphics.strokeWeight(lineWidth)
        graphics.line(startX, startY, endX, endY)
    }

    private fun newX(startX: Float, length: Float, angle: Float): Float =
        startX + length * Math.cos(Math.toRadians(angle.toDouble())).toFloat()

    private fun newY(startY: Float, length: Float, angle: Float): Float =
        startY + length * Math.sin(Math.toRadians(angle.toDouble())).toFloat()
}

data class LSystemSettings(
    val angleOffset: Float,
    val lineWidthOffset: Float,
    val lineLengthScalar: Float,
    val turningAngleOffset: Float
)

data class LSystemState(
    var turningAngle: Float,
    var currentAngle: Float,
    var currentX: Float,
    var currentY: Float,
    var currentLineWidth: Float,
    var currentLineLength: Float,
    var normalMeaning: Boolean
)
