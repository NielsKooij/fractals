package com.nielskooij.fractals.lsystems

import processing.core.PGraphics

fun leaf(graphics: PGraphics, length: Float): LSystem =
        LSystem(graphics,
            hashMapOf(
                "F" to ">F<",
                "a" to "F[+x]Fb",
                "b" to "F[-y]Fa",
                "x" to "a",
                "y" to "b"
            ),
            length,
            "F[+x]Fb",
            LSystemSettings(
                45f,
                1f,
                1.36f,
                0f
            )
        )

fun simpleSquare(graphics: PGraphics, length: Float): LSystem =
    LSystem(graphics,
        hashMapOf(
            "F" to "F+F-F-FF+F+F-F"
        ),
        length,
        "F+F+F+F",
        LSystemSettings(
            90f,
            0f,
            0f,
            0f
        )
    )

fun chaosSquare(graphics: PGraphics, length: Float): LSystem =
    LSystem(graphics,
        hashMapOf(
            "F" to "FF+F-F+F+FF"
        ),
        length,
        "F+F+F+F",
        LSystemSettings(
            90f,
            0f,
            0f,
            0f
        )
    )

fun tree(graphics: PGraphics, length: Float): LSystem =
    LSystem(graphics,
        hashMapOf(
            "F" to "FF",
            "X" to "F[+X]F[-X]+X"
        ),
        length,
        "X",
        LSystemSettings(
            20f,
            0f,
            0f,
            0f
        )
    )

fun tree2(graphics: PGraphics, length: Float): LSystem =
    LSystem(graphics,
        hashMapOf(
            "F" to "FF+[+F-F-F]-[-F+F+F]"
        ),
        length,
        "F",
        LSystemSettings(
            22.5f,
            0f,
            0f,
            0f
        )
    )
