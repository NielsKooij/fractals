package com.nielskooij.fractals.fractalgeometry

import processing.core.PGraphics

class HTree(
    private val graphics: PGraphics,
    private val lineLength: Float,
    private val scalar: Float) {

    fun draw(maxDepth: Int) {
        val sX: Float = graphics.width / 2f
        val sY: Float = graphics.height / 2f + 0.5f * lineLength
        val eX: Float = graphics.width / 2f
        val eY: Float = graphics.height / 2f - 0.5f * lineLength

        graphics.line(sX, sY, eX, eY)
        drawHTreeLine(sX, sY, 0.5f * lineLength, false, 0, maxDepth)
        drawHTreeLine(eX, eY, 0.5f * lineLength, false, 0, maxDepth)
    }

    private fun drawHTreeLine(x: Float, y: Float, length: Float, isVertical: Boolean, depth: Int, maxDepth: Int) {
        if(depth < maxDepth) {
            val sX: Float = if (isVertical) x else x - length * scalar
            val sY: Float = if (isVertical) y - length * scalar else y
            val eX: Float = if (isVertical) x else x + length * scalar
            val eY: Float = if (isVertical) y + length * scalar else y

            graphics.line(sX, sY, eX, eY)
            drawHTreeLine(sX, sY, 0.5f * length, !isVertical, depth + 1, maxDepth)
            drawHTreeLine(eX, eY, 0.5f * length, !isVertical, depth + 1, maxDepth)
        }
    }
}