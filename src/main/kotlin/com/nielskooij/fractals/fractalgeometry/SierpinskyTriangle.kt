package com.nielskooij.fractals.fractalgeometry

import processing.core.PGraphics

class SierpinskyTriangle(private val graphics: PGraphics) {

    fun draw(maxDepth: Int) {
        drawTriangle(0.0f, 0.0f, 0, graphics.width.toFloat(), maxDepth)
    }

    private fun drawTriangle(startX: Float, startY: Float, depth: Int, length: Float, maxDepth: Int) {
        if(depth < maxDepth) {
            drawTriangle(startX, startY, length)

            drawTriangle(startX, startY, depth + 1, length * 0.5f, maxDepth)
            drawTriangle(startX + 0.5f * length, startY, depth + 1, length * 0.5f, maxDepth)
            drawTriangle(startX + 0.25f * length, startY + (0.5f * length * Math.sin(Math.toRadians(60.0)).toFloat()), depth + 1, length * 0.5f, maxDepth)
        }
    }

    private fun drawTriangle(startX: Float, startY: Float, length: Float) {
        graphics.triangle(
            startX, startY,
            startX + length, startY,
            startX + 0.5f * length, startY + (length * Math.sin(Math.toRadians(60.0)).toFloat())
            )
    }
}