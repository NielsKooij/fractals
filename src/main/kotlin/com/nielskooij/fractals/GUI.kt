package com.nielskooij.fractals

import com.nielskooij.fractals.fractalgeometry.SierpinskyTriangle
import com.nielskooij.fractals.lsystems.*
import processing.core.PApplet
import processing.event.KeyEvent
import java.awt.Color

class GUI : PApplet() {

    lateinit var fractal: LSystem
    var maxDepth = 0
    var shouldUpdate = true

    companion object {
        fun run() {
            val gui = GUI()
            gui.runSketch()
        }
    }

    override fun setup() {
        fractal = tree2(graphics, 10f)//simpleSquare(graphics, 10f)
    }

    override fun settings() {
        size(1000, 800)
        super.settings()
    }

    override fun draw() {
        if(shouldUpdate) {
            background(Color.WHITE.rgb)
            fractal.draw(maxDepth, width / 2f, height.toFloat())
            shouldUpdate = false
        }
    }

    override fun keyPressed(event: KeyEvent?) {
        when(event?.keyCode) {
            61 -> {
                maxDepth++
                shouldUpdate = true
                println("MaxDepth: $maxDepth")
            }
            45 -> {
                shouldUpdate = true
                maxDepth = Math.max(0, maxDepth - 1)
                println("Maxdepth: $maxDepth")
            }
        }
    }
}

fun main() {
    GUI.run()
}
